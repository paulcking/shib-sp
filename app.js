var http = require('http');
var fs = require('fs');
var express = require("express");
var dotenv = require('dotenv');
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var session = require('express-session');
var passport = require('passport');
var saml = require('passport-saml');
var util = require('util')

dotenv.load();

passport.serializeUser(function(user, done) {
  done(null, user);
});

passport.deserializeUser(function(user, done) {
  done(null, user);
});

var eduPersonPrincipalName = null;


var samlStrategy = new saml.Strategy({
  callbackUrl: process.env.CALLBACK_URL, // IdP to SP
  entryPoint: process.env.ENTRY_POINT, // SP to IdP
  issuer: process.env.ISSUER, // identifies this sp - usually /shibboleth
  identifierFormat: null,
  decryptionPvk: fs.readFileSync(__dirname + '/cert/key.pem', 'utf8'), // sp private key
  privateCert: fs.readFileSync(__dirname + '/cert/key.pem', 'utf8'), // sp certificate
  cert: fs.readFileSync(__dirname + '/cert/idp_cert.pem', 'utf8'), // idp public key
  validateInResponseTo: false,
  disableRequestedAuthnContext: true
}, function(profile, done) {

    eduPersonPrincipalName = profile['urn:oid:1.3.6.1.4.1.5923.1.1.1.6'];
    var d = {
	id: profile['urn:oid:0.9.2342.19200300.100.1.1'],
	eduPersonPrincipalName: profile.eduPersonPrincipalName,
        email: profile.email,
        displayName: profile.cn,
        firstName: profile.givenName,
        lastName: profile.sn
    }
    //console.log(d);
    console.log(profile);
    
    return done(null, profile); 
});

passport.use(samlStrategy);

var app = express();

app.use(cookieParser());
app.use(bodyParser());
app.use(session({secret: process.env.SESSION_SECRET}));
app.use(passport.initialize());
app.use(passport.session());

function ensureAuthenticated(req, res, next) {
  if (req.isAuthenticated())
    return next();
  else
    return res.redirect('/login');
}

app.get('/',
  ensureAuthenticated, 
  function(req, res) {
    res.send('Authenticated');
  }
);

app.get('/login',
  passport.authenticate('saml', { failureRedirect: '/login/fail' }),
  function (req, res) {
      console.log('/login');
      res.redirect('/');
  }
);

// called by IdP - authenticate and redirect with eppn if successful
app.post('/login/callback',
   passport.authenticate('saml', { failureRedirect: '/login/fail' }),
  function(req, res) {
      //console.log(util.inspect(req, {showHidden: false, depth: null}))
      console.log('/login/callback');
      res.redirect('/?eppn=' + eduPersonPrincipalName);
  }
);

app.get('/login/fail', 
  function(req, res) {
      console.log('in the fail method');
    res.status(401).send('Login failed');
  }
);

// hit this endpoint to generate info IdP will need to register this sp
app.get('/Shibboleth.sso/Metadata', 
  function(req, res) {
    res.type('application/xml');
    res.status(200).send(samlStrategy.generateServiceProviderMetadata(fs.readFileSync(__dirname + '/cert/cert.pem', 'utf8')));
  }
);

// general error handler
app.use(function(err, req, res, next) {
  console.log("Fatal error: " + JSON.stringify(err));
  next(err);
});

var server = app.listen(80, function () {
  console.log('Listening on port %d', server.address().port)
});
